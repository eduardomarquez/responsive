import React, {Component} from 'react';
import Counter from './Counter';

class Product extends Component{
	constructor(props){
		super(props);
        this.state = {
            selectedProduct: {}            
        }
    }    
    render(){
        let image = this.props.image;
        let name = this.props.name;
        let price = this.props.price;
        let id = this.props.id;
        let quantity = this.props.productQuantity;
        return(
            <div className="product">
                <div className="product-image">
                    <img src={image} alt={this.props.name}/>
                </div>                                
                <Counter productQuantity={quantity} updateQuantity={this.props.updateQuantity}/>                                
                <p className="product-price">${this.props.price}</p>                                                                  
                <h4 className="product-name">{this.props.name}</h4>                
            </div>
        )
    }
}
export default Product;
