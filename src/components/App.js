import React, { Component } from 'react';
import './../App.css';
import axios from 'axios';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Products from '../components/Products';

class App extends Component {
  constructor(){
		super();
		this.state = {
			products: [],
			cart: [],			
			quantity : 1,				
		};						
		this.checkProduct = this.checkProduct.bind(this);
		this.updateQuantity = this.updateQuantity.bind(this);				
	}
  	getProducts(){		
		const url = 'products.json';		
		axios.get(url)
			.then(response => {
				this.setState({
					products:response.data.data.items
				})
			})
	}
	componentWillMount(){	
		this.getProducts();		
	}
	handleAddToCart(selectedProducts){		
		let cartItem = this.state.cart;
		let productID = selectedProducts.data.items.id;
		let productQty = selectedProducts.quantity;
		if(this.checkProduct(productID)){
			console.log('hi');
			let index = cartItem.findIndex((x => x.id == productID));
			cartItem[index].quantity = Number(cartItem[index].quantity) + Number(productQty);
			this.setState({
				cart: cartItem
			})
		} else {
			cartItem.push(selectedProducts);
		}
		this.setState({
			cart : cartItem,
			cartBounce: true,
		});
		setTimeout(function(){
			this.setState({
				cartBounce:false,
				quantity: 1
			});
			console.log(this.state.quantity);
			console.log(this.state.cart);
    }.bind(this),1000);  
		this.sumTotalItems(this.state.cart);
		this.sumTotalAmount(this.state.cart);
	}
	handleRemoveProduct(id, e){
		let cart = this.state.cart;
		let index = cart.findIndex((x => x.id == id));
		cart.splice(index, 1);
		this.setState({
			cart: cart
		})
		this.sumTotalItems(this.state.cart);
		this.sumTotalAmount(this.state.cart);
		e.preventDefault();
	}
	checkProduct(productID){
		let cart = this.state.cart;
		return cart.some(function(item) {
			return item.id === productID;
		}); 
	}
	//Reset Quantity
	updateQuantity(qty){
		console.log("quantity added...")
		this.setState({
				quantity: qty
		})
	}
  render() {
    return (
			<div className="container">
			<Header
			cartBounce={this.state.cartBounce}
			total={this.state.totalAmount}
			totalItems={this.state.totalItems}
			cartItems={this.state.cart}
			removeProduct={this.handleRemoveProduct}
			updateQuantity={this.updateQuantity}
			productQuantity={this.state.moq}
			/>
			<Products
			productsList={this.state.products}
			addToCart={this.handleAddToCart}
			productQuantity={this.state.quantity}
			updateQuantity={this.updateQuantity}			
			/>
			<Footer />
			</div>
    );
  }
}

export default App;
