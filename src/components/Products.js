import React, {Component} from 'react';
import Product from './Product';
import LoadingProducts from '../loaders/Products';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

class Products extends Component{
	constructor(){
		super();
	}
  	render(){
    	let productsData;
    	let x;
		productsData = this.props.productsList.map(product =>{
			return(
					<Product 
					key={product.id} 
					price={product.price} 
					name={product.name} 
					image={product.img} 
					id={product.id} 
					addToCart={this.props.addToCart} 
					productQuantity={this.props.quantity} 
					updateQuantity={this.props.updateQuantity} 
					openModal={this.props.openModal}/>
				)
			}
		);		
		// Empty and Loading States
		let view;
		
		view = <CSSTransitionGroup
			transitionName="fadeIn"
			transitionEnterTimeout={500}
			transitionLeaveTimeout={300} 
			component="div"
			className="products">
				{productsData}
			</CSSTransitionGroup>
		
		return(
			<div className="products-wrapper">
				{view}
			</div>
		)
	}
}

export default Products;